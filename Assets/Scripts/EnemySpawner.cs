using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    [SerializeField]
    EnemyGroup[] groups;

    int prev = -1;

    void Start() {
        Spawn();
    }

    public void Spawn() {
        int i = Random.Range(0, groups.Length);
        // Reroll i once to lower odds of getting same pattern consecutively 
        if(i == prev) {
            i = Random.Range(0, groups.Length);
        }

        // Spawn off a random side of the screen
        Vector3 position = new Vector3(0, 0, 0);
        switch (Random.Range(0,3)) {
            case 0:
                position = new Vector3(-120, 10, 45);
                break;
            case 1:
                position = new Vector3(120, 10, 45);
                break;
            case 2:
                position = new Vector3(0, 150, 45);
                break;
            default:
                Debug.Log("Error in Spawn Switch");
                break;
        }
        GameObject group = Instantiate(groups[i].gameObject, position, new Quaternion());
        group.transform.parent = transform;
        prev = i;
    }

    public void GameOver() {
        Destroy(gameObject);
    }
}
