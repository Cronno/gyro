using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    int damage = 2;

    float speed = 6000f;

    Vector3 direction;

    public void Shoot(Vector3 direction) {
        GetComponent<Rigidbody>().AddForce(direction * speed);
    }

    void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.tag != "Player") {
            Destroy(gameObject);
        }
        if(collision.gameObject.tag == "Enemy") {
            collision.gameObject.SendMessage("TakeDamage", damage);
        }
    }
}
