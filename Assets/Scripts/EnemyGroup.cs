using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGroup : MonoBehaviour {

    // Position the group moves to after spawning
    Vector3 targetPostion = new Vector3(0, 18, 40);

    int enemiesLeft = 0;

    void Start() {
        foreach(Transform child in transform) {
            Enemy enemy = child.gameObject.GetComponent<Enemy>();
            // Enemy movement speed and max distace from origin are randomized
            float speed = Random.Range(0, 0.1f);
            enemy.SetSpeed(speed);
            float maxDistance = Random.Range(10, 30);
            enemy.SetDistance(maxDistance);
            enemiesLeft += 1;
        }
    }

    void Update() {
        if(transform.position != targetPostion) {
            transform.position = Vector3.MoveTowards(transform.position, targetPostion, 0.6f);
        }
        if(enemiesLeft <= 0) {
            transform.parent.GetComponent<EnemySpawner>().Spawn();
            Destroy(gameObject);
        }
    }
    
    public void EnemyDefeated() {
        enemiesLeft -= 1;
    }
}
