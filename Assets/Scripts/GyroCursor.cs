﻿using UnityEngine;
using UnityEngine.UI;

public class GyroCursor : MonoBehaviour {

    [SerializeField, Range(0f,1f)]
    float xGyroRange = 4.0f;
    [SerializeField, Range(0f,1f)]
    float yGyroRange = 4.0f;

    [SerializeField, Range(0f,10f)]
    float minMovement = 3f;

    public GameObject player;
    Gyroscope Gyro;
    Vector3 gyroCenter;

    void Start() {
        Gyro = Input.gyro;
        Gyro.enabled = true;
        Recenter();
    }

    void FixedUpdate() {
        CastRay();
        float xCenter = Screen.width/2;
        float yCenter = Screen.height/2;
        
        float xGyro = gyroCenter.x - Gyro.gravity.x;
        float xPos = xCenter * (xGyro/xGyroRange) + xCenter;

        float yGyro = gyroCenter.y - Gyro.gravity.y;
        float yPos = yCenter * (yGyro/yGyroRange) + yCenter;

        // Prevent the cursor from moving off screen
        xPos = Mathf.Max(xPos, 0);
        xPos = Mathf.Min(xPos, Screen.width);
        yPos = Mathf.Max(yPos, 0);
        yPos = Mathf.Min(yPos, Screen.height);

        // Remove jitter
        if(Mathf.Abs(xPos-transform.position.x) < minMovement) {
            xPos = transform.position.x;
        }
        if(Mathf.Abs(yPos-transform.position.y) < minMovement) {
            yPos = transform.position.y;
        }

        transform.position = new Vector3(xPos, yPos, 0);
    }

    void CastRay() {
        Ray ray = Camera.main.ScreenPointToRay(transform.position);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit) && hit.collider.tag == "Enemy") {
            // Debug.Log("Hit: " + hit.collider.name);
            player.SendMessage("Aim", hit.point);
        }
    }

    public void Recenter() {
        gyroCenter = Gyro.gravity;

        // Prevent the gyro from recentering vertically in a place where the max value is unreachable 
        if(gyroCenter.y - yGyroRange < -1.0f) {
            gyroCenter.y = -1.0f + yGyroRange;
        }
    }

    // void OnGUI() {
    //     GUI.Label(new Rect(100, 50, 200, 40), "Gyro attitude" + Gyro.attitude.eulerAngles);
    //     GUI.Label(new Rect(100, 100, 200, 40), "PosX : " + transform.position.x);
    //     GUI.Label(new Rect(100, 150, 200, 40), "PosY : " + transform.position.y);
    //     GUI.Label(new Rect(100, 200, 200, 40), "gyroCenter : " + gyroCenter);
    //     GUI.Label(new Rect(100, 250, 200, 40), "Gravity : " + Gyro.gravity);
    // }
}