using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField]
    float speed = 0.1f;

    [SerializeField]
    Bullet bulletPrefab;

    [SerializeField]
    GameObject hpText;

    int hitpoints = 100;

    bool leftHeld = false;
    bool rightHeld = false;

    void FixedUpdate() {
        float direction = 0;
        if(leftHeld) {
            direction -= 1;
        }
        if(rightHeld) {
            direction += 1;
        }
        Vector3 deltaPos = new Vector3(speed * direction, 0, 0);
        if(Mathf.Abs(transform.position.x + deltaPos.x) < 19) {
            transform.position += deltaPos;
        }
    }

    public void RButtonState(bool state) {
        rightHeld = state;
    }

    public void LButtonState(bool state) {
        leftHeld = state;
    }

    void Aim(Vector3 target) {
        Bullet b = Instantiate(bulletPrefab.gameObject, transform.position, new Quaternion()).GetComponent<Bullet>();
        b.Shoot((target - transform.position).normalized);
        Debug.DrawLine(transform.position, target, Color.red);
    }

    void TakeDamage(int damage) {
        hitpoints -= damage;
        hpText.SendMessage("UpdateValue", hitpoints);
        if(hitpoints <= 0) {
            Destroy(gameObject);
            GameOver();
        }
    }

    void GameOver() {
        EnemySpawner spawner = GameObject.Find("Enemy Spawner").GetComponent<EnemySpawner>();
        spawner.GameOver();
        GameObject gameOver = GameObject.Find("Game Over");
        foreach(Transform child in gameOver.transform) {
            child.gameObject.SetActive(true);
        }
    }
}
