using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIText : MonoBehaviour {

    [SerializeField]
    string prefix = "";

    [SerializeField]
    int value = 0;

    void UpdateValue(int n) {
        value = n;
        UpdateDisplay();
    }

    void IncrementValue(int n) {
        value += n;
        UpdateDisplay();
    }

    void UpdateDisplay() {
        gameObject.GetComponent<Text>().text = prefix + " " + value;
    }
}
