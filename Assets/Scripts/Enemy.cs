using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField]
    int hitpoints = 100;

    [SerializeField]
    EnemyBullet bulletPrefab;

    // Fire rate is time in seconds between shots
    [SerializeField]
    float fireRate = 2;

    Transform player;
    UIText score;

    float speed = 0.1f;

    Vector3 direction = new Vector3(1,0,0);
    Vector3 startingPosition;
    float maxDistanceFromStart = 20f;

    float shotCooldown;

    void Start() {
        startingPosition = transform.localPosition;
        shotCooldown = fireRate + Random.Range(0,1f);
        if(Random.Range(0,2) == 1) {
            direction.x *= -1;
        }
        player = GameObject.Find("Player").transform;
        score = GameObject.Find("Score").transform.GetComponent<UIText>();
    }

    void Update() {
        shotCooldown -= Time.deltaTime;
        if(shotCooldown <= 0) {
            Shoot();
        }

        if(hitpoints <= 0) {
            transform.parent.gameObject.GetComponent<EnemyGroup>().EnemyDefeated();
            Destroy(gameObject);
        }
    }

    void FixedUpdate() {
        Move();
    }

    void TakeDamage(int damage) {
        hitpoints -= damage;
        score.SendMessage("IncrementValue", 100);
    }

    void Move() {
        // Enemies move along the X-axis, but never stray too far from their starting point
        if(Vector3.Distance(transform.localPosition, startingPosition) > maxDistanceFromStart) {
            direction *= -1;
        }
        transform.localPosition += direction * speed;
    }

    void Shoot() {
        // Create a new bullet and fire it at the player's current position
        EnemyBullet b = Instantiate(bulletPrefab.gameObject, transform.position, new Quaternion()).GetComponent<EnemyBullet>();
        Vector3 target = player.position;
        target.x += Random.Range(-3f,3f);
        b.Shoot((target - transform.position).normalized);
        shotCooldown = fireRate;
        Debug.DrawLine(transform.position, target, Color.red);

    }

    public void SetSpeed(float s) {
        speed = s;
    }

    public void SetDistance(float d) {
        maxDistanceFromStart = d;
    }
}
