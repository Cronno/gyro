using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    float speed = 800;

    public void Shoot(Vector3 direction) {
        GetComponent<Rigidbody>().AddForce(direction * speed);
    }

    void OnTriggerEnter(Collider collider) {
        if(collider.gameObject.tag != "Enemy" && collider.gameObject.tag != "Bullet") {
            Destroy(gameObject);
        }
        if(collider.gameObject.tag == "Player") {
            collider.gameObject.SendMessage("TakeDamage", 10);
        }
    }
}
